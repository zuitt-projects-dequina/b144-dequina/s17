console.log("Array Manipulation")

//Basic Array Structure
//Access Elements in an Array - thru index

// Two ways to initialize an Array (Syntax)

	// #1	let array = []
	// #2 	let arr = new Array();


let array = [1, 2, 3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr);

console.log(array[0]);

let count = ['one', 'two', 'three', 'four']
	// using assignment operator (=)
	count[4] = "five";
	console.log(count);

	// Push Method array.push()
		// element is added at the end of an array
	count.push("element")
	console.log(count)

	function pushMethod(element) {
		return count.push(element)
	}

	pushMethod("six")
	pushMethod("seven")
	pushMethod("eight")

	// Pop Method Array array.pop()
		// removes the last element of an array

		function popMethod() {
			return count.pop(); // no need to add paraments, because popMethod dont accepts parameters
		}


	// Unshift Method 	array.unshift()
		// adds element at the beginning of an array

	count.unshift("hugot")
	console.log(count)

	function unshiftMethod(name) {
		return count.unshift(name)
	}

	unshiftMethod("zero")
	console.log(count)


	// Shift Method 	array.shift()
		// removes element at the beginning of an array

	count.shift()
	console.log(count)

	function shiftMethod() {
		return count.shift() //same as pop, only the first element is being remove, no need for parameters
	}

	shiftMethod()
	console.log(count)


	// Sort Method 		array.sort()
		
	let nums = [15, 32, 61, 130, 230, 13, 34]
	nums.sort();
	console.log(nums)

		// sort nums in ascendting order
		nums.sort(
			function(a, b) {
				return a - b
			}
		);
		console.log(nums);

		// sort nums in descendting order
		nums.sort(
			function(a, b) {
				return b - a
			}
		);
		console.log(nums);


		// Reverse Method 		array.reverse()
		nums.reverse()
		console.log(nums) // just reversing an array to what it is, not sorting it


	// Splice and Slice 

		// Splice Method 		array.splice()
			// returns an array of omitted elements
			// it directly manipulates the original array

			// first parameter - index where it start omitting element
			// second parameter - # of elements to be omitted starting from first parameter
			// thind parameter - elemets to be added in place of the omitted elemets
		console.log(count)

		// let newSplice = count.splice(1);
		// console.log(newSplice);
		// console.log(count);

		// let newSplice = count.splice(1,2);
		// console.log(newSplice);
		// console.log(count)

		// let newSplice = count.splice(1, 2, "a1", "b2", "c3")
		// console




		// Slice Method 		array.slice(start,end,)
			// does not affect the original array direclty, just a mere photocopy of the array, and there it manipulates

			// first parameter - index where to begin omitting elements
			// second parameter - number of elements to be omitted (index - 1)

		console.log(count)

		// let newSlice = count.slice(1);
		// console.log(newSlice);
		// console.log(count)

		let newSlice = count.slice(2, 3);
		console.log(newSlice)
		console.log(count)

		// Concat Method 		array.concat()
				// used to merge two or more arrays
		console.log(count)
		console.log(nums)
		let animals = ["bird", "cat", "dog", "fish"];

		let newConcat = count.concat(nums, animals);
		console.log(newConcat);

		// Join Method 			array.join()
				//to join element to form 1 string
				// parameters "", "-", " "

		let meal = ["rice", "steak", "juice"];

		let newJoin = meal.join() // default separator is comma
		console.log(newJoin)

		newJoin = meal.join("") // no space
		console.log(newJoin)

		newJoin = meal.join(" ") // with space
		console.log(newJoin)

		newJoin = meal.join("-") // with hypen
		console.log(newJoin)


		// toString Method 
				// converting 

		console.log(nums)
		console.log(typeof nums[3]) // to check the type of an element

		let newString = nums.toString()
		console.log(newString)
		console.log(typeof newString)


	// Accessors

	let countries = ["US", "PH", "CAN", "SG", "HK", "PH", "NZ"];

		// indexOf()		array.indexOf()
			// finds the index of a given element where it is "first" found

		let index = countries.indexOf("PH");
		console.log(index) // returns 1 (type: number)

		// lastIndexOf()
			// finds the index of a given element where it is "last" found
		let lastIndex = countries.lastIndexOf("PH");
		console.log(lastIndex); //returns 5 

			//if element is non-existing, return is -1
			lastIndex = countries.lastIndexOf("AU");
			console.log(lastIndex); //returns -1 


			//example:
				// if(countries.indexOf("AU") == -1){
				// 	console.log("Element not existing")
				// }
				// else {
				// 	console.log("Element exist on array")
				// }


	// Iterators

		// forEach()		array.forEach()
		// map()			array.map()


	let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

		//forEach
			//returns undefined
		days.forEach(
			function (element) {
				console.log(element)
			}
		)

	//map
		// return a copy of an array fromt the original array which can be manipulated
	let mapDays = days.map(function(day){
			return `${day} is the day of the week`
		}
	)

	console.log(mapDays)
	console.log(days)


console.log("/////////// Mini-Activity");

	days2 = [];

	days.forEach(
		function (element) {
			return days2.push(element);
		}
	)

	console.log(days)




	// filter 		array.filter(cb())

	console.log(nums)

	let newFilter = nums.filter(function(num){
		// logic
		if (num < 50) {
		return num;
	}
	})

	console.log(typeof newFilter) // returns
	console.log(newFilter) // returns


	//includes 		array.includes()
		//returns boolean value if a word is existing in an array
	console.log(animals)

	function miniActivity(name) {

		if(animals.includes(name) == true) {
			return console.log(`${name} is found`)
		} else {
			return console.log(`${name} is not found`)
		}
	}

	miniActivity("dog")
	miniActivity("Snake")
	miniActivity("cat")


	// every(cb())
		// returns boolean
		// returns true only if "all elements" passed the given condition
		// same as how AND (&&) works

	let newEvery = nums.every(function(num){
		return (num > 50)
	})

	console.log(newEvery);


	// some(cb())
		// returns boolean
		// returns true if "one of the elements" passed the given condition
		// same as how OR || works
	let newSome = nums.some(function (num) {
		return (num > 50)
	})
	console.log(newSome);



	// reduce(cb(<previous value>, <current value>))
		// performs arithmetic operations to all elements

	let newReduce = nums.reduce(function (a, b) {
		return b - a
	})
	console.log(newReduce)


