// Array Manipulation Activity

let students = [];

function addStudent(name) {
	console.log(`${name} was added to student's list.`)
	return students.push(name.toLowerCase());
}

function countStudents() {
	if (students.length > 1) {
		return console.log(`There are a total of ${students.length} students enrolled.`)
	} else {
		return console.log(`There is a total of ${students.length} student enrolled.`)
	}
}

function printStudents(name) {
	students.sort().forEach( 
		function (name) {
			console.log(name);
		}
	)
}


function findStudent(name) {
	if (students.includes(name.toLowerCase()) == true) {
		console.log(`${name} is an Enrollee.`)
	}

	else {
		console.log(`No student found with the name ${name}.`)
	}
}

